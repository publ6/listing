import { tableData } from './data.js';

//CONSTRUCT TABLE
var tableBody = document.getElementById('result-table');

tableData.forEach(function (rowData) {
    var row = document.createElement('tr');
    row.className = 'result';

    rowData.forEach(function (cellData) {
        var cell = document.createElement('td');
        if (cellData.startsWith("src=")) {
            var img = document.createElement('img');
            img.src = cellData.substring(4);
            img.classList.add('picture');
            cell.appendChild(img);
            row.appendChild(cell);
        }
        else if (cellData == "non") {
            row.classList.add('available');
        }
        else if (cellData == "oui") {
            row.classList.add('reserved');
        }
        else {
            cell.appendChild(document.createTextNode(cellData));
            row.appendChild(cell);
        }
    });

    tableBody.appendChild(row);
});


// FILTER
let filterInput = document.querySelector('.filter-input');
const result = document.querySelectorAll('.result');

filterInput.addEventListener('keyup', () => {
    let criteria = filterInput.value.toUpperCase().trim();

    result.forEach((data) => {
        if (data.innerText.toUpperCase().indexOf(criteria) > -1) {
            data.style.display = '';
        } else {
            data.style.display = 'none';
        }
    });
});


// SORT
const th = document.querySelectorAll('thead th');
const tbody = document.querySelector('tbody');

let sortDirection;

th.forEach((col, idx) => {
    col.addEventListener('click', () => {
        sortDirection = !sortDirection;

        const rowsArrFromNodeList = Array.from(result);
        const filteredRows = rowsArrFromNodeList.filter(item => item.style.display != 'none');

        filteredRows.sort((a, b) => {

            if (a.childNodes[idx].getElementsByTagName('*').length > 0)
            {
                var aChild = a.childNodes[idx].children[0]
                var bChild = b.childNodes[idx].children[0]
                return aChild.innerHTML.localeCompare(bChild.innerHTML, 'en', { numeric: true, sensitivity: 'base' });
            }
            else
            {
                return a.childNodes[idx].innerHTML.localeCompare(b.childNodes[idx].innerHTML, 'en', { numeric: true, sensitivity: 'base' });
            }
            
        }).forEach((row) => {
            sortDirection ? tbody.insertBefore(row, tbody.childNodes[tbody.length]) : tbody.insertBefore(row, tbody.childNodes[0]);
        });

    });
});

const button = document.querySelector('#hide-show');
button.addEventListener("click", () => {
    var list = Array.from(document.getElementsByClassName('reserved'));

    list.forEach(element => {
        if (element.style.display === "none") {
            element.style.display = "table-row";
        } else {
            element.style.display = "none";
        }
    });
});
